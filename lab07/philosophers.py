import threading
import time
import random


class Philosopher(threading.Thread):
    #Each philosopher has a fork on both sides as he needs both to eat but only not everyone can at the same time
    #To reapir it we need to use threading and 
    def __init__(self, philosopher_number, lfork, rfork):
        super().__init__()
        self.philosopher_number = philosopher_number
        self.lfork = lfork
        self.rfork = rfork

    def try_to_eat(self):
        self.lfork.acquire()
        self.rfork.acquire()

        print(f"{self.philosopher_number} has started eating")
        time.sleep(1)
        print(f"{self.philosopher_number} has stopped eating")

        self.lfork.release()
        self.rfork.release()

    def run(self):
        while True:
            print(f"{self.philosopher_number} is thinking")
            time.sleep(random.randint(1, 5))
            self.try_to_eat()

def main():

    fork = [threading.Condition(threading.Lock()) for i in range(5)]
    philosophers = [Philosopher(i, fork[i], fork[(i + 1) % 5]) for i in range(5)]

    for philo in philosophers:
        philo.start()

if __name__ == '__main__':
    main()
