import concurrent.futures
import random
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter


def lottery():

    return random.randint(0, 10)


def histogram(num_runs=1000000):

    combination = np.ones(num_runs, dtype=int)
    for i in range(num_runs):
        #Checking different possibilities of distribiution
        #combination[i] = lottery() * lottery()
        #combination[i] = lottery() + lottery()
        combination[i] = lottery() - lottery()
        #combination[i] = lottery() ** lottery()

    counter = Counter()
    for result in combination:
        counter[result] = counter[result] + 1

    sorted_counter = {x: y for x, y in sorted(list(counter.items()))}

    x = list(sorted_counter.keys())
    y = list(sorted_counter.values())

    return x, y


def main():
    with concurrent.futures.ThreadPoolExecutor(max_workers=2) as thread:
        th = thread.submit(histogram, 1000000)

    x_axis, y_axis = th.result()

    fig, ax = plt.subplots()
    ax.stem(x_axis, y_axis)
    ax.set_title("Combining 0-10 lotteries")
    plt.show()


if __name__ == '__main__':
    main()