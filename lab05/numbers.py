import cmath


class Complex:

    def __init__(self, real, imag=0.0):
        self.real = real
        self.imag = imag

    @classmethod
    def from_string(cls, number_as_str):

        new_str = None
        sign = None
        signs = ['+', '-']
        for idx, char in enumerate(number_as_str):
            if char in signs:
                sign = number_as_str[idx]
                new_str = number_as_str.replace(sign, " ")
                break

        real, imag = new_str.split()
        imag = imag[:-1]
        real, imag = float(real), float(imag)
        imag = imag if sign == '+' else -imag
        return cls(real, imag)

    def __repr__(self):
        return f"({self.real}{'+' if self.imag >= 0 else '-'}{abs(self.imag)}j)"

    def __add__(self, other):
        if isinstance(other, (float, int)):
            other = Complex(other)
        return Complex(self.real + other.real, self.imag + other.imag)

    def __sub__(self, other):
        if isinstance(other, (float, int)):
            other = Complex(other)
        return Complex(self.real - other.real, self.imag - other.imag)

    def __mul__(self, other):
        if isinstance(other, (float, int)):
            other = Complex(other)
        return Complex(self.real * other.real - self.imag * other.imag,
                             self.imag * other.real + self.real * other.real)

    def __truediv__(self, other):
        if isinstance(other, (float, int)):
            other = Complex(other)
        divisor = (other.real ** 2 + other.imag ** 2)
        return Complex((self.real * other.real) - (self.imag * other.imag) / divisor,
                             (self.imag * other.real) + (self.real * other.imag) / divisor)
