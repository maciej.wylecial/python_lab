import numpy as np
import random
import copy

def smaller_matrix(bigger_matrix, row, col):
    new_matrix = copy.deepcopy(bigger_matrix)
    new_matrix.remove(bigger_matrix[row])
    for idx in range(len(new_matrix)):
        new_matrix[idx].pop(col)
    return new_matrix

def determinant(matrix):
    row = len(matrix)
    col = len(matrix[0])
    if row != col:
        raise ValueError("Matrix isn't square")

    if len(matrix) == 1:
        return matrix[0][0]
    else:
        det = 0
        for i in range(col):
            det += ((-1) ** i) * matrix[0][i] * determinant(smaller_matrix(matrix, 0, i))
        return det

def main():
    dim = np.random.randint(1, 4)
    a = [[random.random() for _ in range(dim)] for _ in range(dim)]
    print(f"Matrix size: {dim}x{dim}")
    det_a = determinant(a)
    if np.allclose(det_a, np.linalg.det(a)):
        print(f"Determinant: {det_a}")
    else:
        print("Error")

if __name__ == '__main__':
    main()
    
