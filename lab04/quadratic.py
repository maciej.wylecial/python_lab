import math
import sys

values = input("Please type parameters: a, b, c: ")
a, b, c = values.split(", ")
a = float(a)
b = float(b)
c = float(c)

delta = math.sqrt((b ** 2) - (4 * a * c))

#If delta is <0 it already breaks
x1 = (-b + delta) / (2 * a)
x2 = (-b - delta) / (2 * a)

if(delta==0):
    print(f"delta is 0, the result is x= {x1}")
else:
    print(f"the results are x1 = {x1}, and x2 = {x2}")