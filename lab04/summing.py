import random
import numpy as np

dim = 128
a = [[random.random() for _ in range(dim)] for _ in range(dim)]
b = [[random.random() for _ in range(dim)] for _ in range(dim)]
for i in range(dim): 
    for j in range(dim):
        result = a[i][j] + b[i][j] 
if np.allclose(result, np.add(a, b)):
    print(result)
else:
    print("Error")
