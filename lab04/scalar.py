import numpy as np

a = [1, 2, 12, 4]
b = [2, 4, 2, 8]

scalar = 0
for u, v in zip(a, b):
    scalar += u * v

if np.allclose(scalar, np.inner(a, b)):
    print(f"Scalar equals: {scalar}")
else:
    print("Error")
