import numpy as np
import random


dim = 8
a = [[random.random() for _ in range(dim)] for _ in range(dim)]
b = [[random.random() for _ in range(dim)] for _ in range(dim)]
result = [[0 for _ in range(dim)] for _ in range(dim)]

row_a = len(a)
col_a = len(a[0])
row_b = len(b)
col_b = len(b[0])

for i in range(row_a):
    for j in range(col_b):
        for k in range(row_b):
            result[i][j] += a[i][k] * b[k][j]

if np.allclose(result, np.array(a) @ np.array(b)):
    print(result)
else:
    print("Error")
