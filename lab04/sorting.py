import random


def sorting(numbers):
    swapping = True

    while swapping:
        swapping = False
        for i in range(len(numbers) - 1):
            if numbers[i] < numbers[i + 1]:
                numbers[i], numbers[i + 1] = numbers[i + 1], numbers[i]
                swapping = True

if __name__ == '__main__':
    randomized = [random.random() for _ in range(50)]
    check = sorted(randomized, reverse=True)
    sorting(randomized)

    if (randomized == check):
        print("Works")
        print(f"After sorting: {randomized}")
    else:
        print("Error")
