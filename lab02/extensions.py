import os.path
from PIL import Image

dir = "images"
jpg = [file for file in os.listdir(dir) if
                  os.path.isfile(os.path.join(dir, file)) and file.endswith(".jpg")]

for i in jpg:
    img = Image.open(os.path.join(dir, i))
    name, ext = i.split(".")
    img.save(os.path.join(dir, f"{name}.png"))