import os.path


def printing(path, depth):
	print("\n" * depth + path)


def recurse(start_dir, depth=0):
    last_element_of_dir = start_dir.split("\\")[-1]
    printing(last_element_of_dir, depth)
    for element in os.listdir(start_dir):
        if not os.path.isdir(os.path.join(start_dir, element)):
            printing(element, depth + 1)
        else:
            recurse(os.path.join(start_dir, element), depth + 1)

recurse("images")
