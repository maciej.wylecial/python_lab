import os.path

path = "/dev"
files = len([file for file in os.listdir(path) if os.path.isfile(os.path.join(path, file))])
if(files <= 1):
    if(files == 0):
        print(f"There is no files in {path} directory")
    else:
        print(f"There is {files} file in {path} directory")
else:
    print(f"There are {files} files in {path} directory")
