import xml.dom.minidom

file = xml.dom.minidom.parse("pog.xml")
buildings = file.documentElement.getElementsByTagName('place')

building = buildings[0].getElementsByTagName('C3')[0].childNodes[0].data
room = buildings[0].getElementsByTagName('309')[0].childNodes[0].data

print(f"Next LAB is at: {building} {room}")

buildings[0].getElementsByTagName('C3')[0].childNodes[0].data = 'C2'
with open('another_wing.xml', 'w+') as new_file:
    new_file.write(file.toxml())
