import csv
import sys
#import pandas as pd

class Data_work:
    def __init__(self, file):
        self.file = file

    def show_data(self):

        #with open(self.file) as f:
            #next(f)
            #data=[tuple(line) for line in csv.reader(f)]

        #frame = pd.DataFrame(data, columns =['Id', 'start', 'stop', 'Size[KB]'])

        with open(self.file) as file:
            csv_reader = csv.reader(file)
            header = next(csv_reader)
            for i, line in enumerate(csv_reader):
                print(f"Row {i + 1}")
                for i, header_row in enumerate(header):
                    print(f"{header_row} - {line[i]}")
                print()

    def new_data(self):
        with open(self.file) as csv_in_file:
            csv_reader = csv.reader(csv_in_file)
            header = next(csv_reader)

        new_row = [input(f"Type new data {header_row}: ") for header_row in header]
        with open(self.file, 'a+', newline='') as output:
            new_data = csv.writer(output)
            new_data.writerow(new_row)

    def remove_data(self):

        remove = int(input("Enter data to delete: "))

        if remove == 0:
            print("Out of range")
            return

        with open(self.file, 'r') as csv_in_file:
            reader = csv.reader(csv_in_file)
            lines = [row for row in reader]

            for i, line in enumerate(lines):
                if remove == i:
                    lines.remove(line)
                    break
            else:
                print("Could not find entered number of record")
                return

        with open(self.file, 'w', newline='') as output:
            writer = csv.writer(output)
            writer.writerows(lines)

    def menu(self):
        option = """
        What would you like to do?
        Show stored data - preview
        Add new data - add
        Remove data - remove
        quit
        """
        action_handler = {
            'preview': self.show_data,
            'add': self.new_data,
            'remove': self.remove_data,
            'quit': sys.exit
        }
        while True:
            user_input = input(option)
            action_handler[user_input]()   

def main():
    startup = Data_work("stats.csv")
    startup.menu()
if __name__ == '__main__':
    main()