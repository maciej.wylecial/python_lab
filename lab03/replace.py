
replace = {
    'i': 'oraz',
    'nigdy': 'prawie nigdy',
    'dlaczego': 'czemu',
    'oraz': 'i',
    'prawie nigdy': 'nigdy',
    'czemu': 'dlaczego'
}

with open("text.txt", 'r') as input:
    input_text = input.read()

new = []
for i in input_text.split():
    if i in replace.keys():
        new.append(replace[i])
    else:
        new.append(i)

print(" ".join(new))

