
cancelled = ['się', 'i', 'oraz', 'nigdy','dlaczego']

with open("text.txt", 'r') as in_file:
    input_text = in_file.read()

input_words = input_text.split()
new_words = [word for word in input_words if word not in cancelled]
new_text = " ".join(new_words)
print(new_text)
